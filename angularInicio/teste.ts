//map -> sempre que precisa fazer manipulação com cada
//elemento de um array. Sem arrow function
// arrayNumeros = [2, 3,4,5,6];
// novoValor = arrayNumeros.map(function(num){
//     return num * 2;
// });
// console.log(novoValor);
//com arrow function
// arrayNumeros = [2, 3,4,5,6];
// novoValor = arrayNumeros.map((num) => num * 2);
// console.log("Novo valor");
// console.log(novoValor);

//Filter usando o include que verifica verdadeiro ou falso parecido com indexOf
//mas sem precisar das validações dele, ele ja faz isso
// let nomes = ['jorge', 'carlos', 'roberto', 'lucas'];
// let novoNome = nomes.filter(function(nome){
//     return nome.includes('a');
// });
// console.log(novoNome);

// nomes = ['jorge', 'carlos', 'roberto', 'lucas'];
// let novoNome2 = nomes.filter((nome) => nome.includes('o'));
// console.log("Novo");
// console.log(novoNome2);

//reduce reduz todo o array para um elemento ou uma string separando por -
// nomes = ['jorge', 'carlos', 'roberto', 'lucas'];
// let novoNome3 = nomes.reduce((acumulado, nome) => acumulado + '-'+nome);
// console.log("com reduce");
// console.log(novoNome3);

// let valores = [10, 20, 30, 40, 50];
// let novoValor = valores.reduce((acumulado, corrente) => acumulado + corrente);
// console.log("reduce com valores");
// console.log(novoValor);
//tipagem
//let nome : string  = 'Ana maria';

// class Teste{
//     nome : string;

//     constructor(nome: string){
//         this.nome = nome;
//     }
//     dizOla():string{
//         return "Olá " + this.nome;
//     }
// }
// var teste = new Teste("Ana");
// console.log(teste);

//aceitar qualquer coisa
// var lista : any[] = [1, true, "angular 2"];
// console.log(lista);

//visibilidade dentro de uma classe
// private nome:string

//funções opcionais e com dois tipos
// function juntarNome(nome: string, sobrenome?: string):string{
//     if(sobrenome){
//         return nome + " " + sobrenome;
//     }
//     else{
//         return nome;
//     }
// }
// let teste =  juntarNome("ana");
// console.log(teste);

//caso queira uma função com parametros diferentes  podendo ser
// uma string ou numero
// function logConsole(log: (string | number)){
//     console.log(log);
// }
// function logConsole2(log2: (string | number), conteudo: (number|any[])){
//     console.log(log2);
//     console.log(conteudo);
// }


